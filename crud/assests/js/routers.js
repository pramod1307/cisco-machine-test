jQuery(document).ready(function () {
    $("#search-router").keyup(function () {
        searchText = $(this).val();
        $.ajax({
            type: "POST",
            url: 'RouterDetails/index?q=' + searchText,
            async: false,
            data: {},
            success: function (data) {
                var html = '';
                var jsonData = JSON.parse(data);
                var counter = 1;
                if (jsonData.length) {
                    $.each(jsonData, function (i, item) {
                        html += "<tr>";
                        html += "<td>" + counter++ + "</td>";
                        html += "<td>" + item.sapid + "</td>";
                        html += "<td>" + item.hostname + "</td>";
                        html += "<td>" + item.loopback + "</td>";
                        html += "<td>" + item.mac_address + "</td>";
                        html += "<td>" + item.created_at + "</td>";
                        html += "<td>" + item.updated_at + "</td>";
                        html += "<td>" + getEditLink(item.id) + "</td>";
                        html += "<td>" + getDeleteLink(item.id) + "</td>";
                        html += "</tr>";
                    });
                } else {
                    var html = '<tr><td colspan="9" style="text-align: center">No record found</td></tr>';
                }


                $("#routerHtmlBody").html(html);
            }
        });
    });
})

function getEditLink(id) {
    return '<a href="RouterDetails/getdetails/' + id + '" class="btn btn-primary btn-xs glyphicon glyphicon-pencil"> </a>';
}

function getDeleteLink(id) {
    return '<a href="RouterDetails/deleteRow/' + id + '" class="glyphicon glyphicon-trash btn-danger btn-xs"> </a>';
}