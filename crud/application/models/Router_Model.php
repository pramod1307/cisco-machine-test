<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Router_Model extends CI_Model {

    public function __constructor() {
        parent::__construct();
    }

    public function getdata($filter = []) {
        
        if(!empty($filter)) {
            if(!is_array($filter)) {
                $filter = (array) $filter;
            }
            
            foreach ($filter As $key => $value) {
                $this->db->or_where($key." LIKE ", '%'.$value.'%');
            }
        }
        
        $query = $this->db->select('id, sapid, hostname, loopback, mac_address, created_at, updated_at')->get('crud');
        return $query->result();
    }

    public function getuserdetail($uid) {
        $ret = $this->db->select('id, sapid, hostname, loopback, mac_address, created_at, updated_at')
                ->where('id', $uid)
                ->get('crud');
        return $ret->row();
    }

    public function insertUpdateData($data, $usid = 0) {

        if ($usid === 0) {
            $sql_query = $this->db->insert('crud', $data);
        } else {
            $data['updated_at'] = date("Y-m-d H:i:s");
            $sql_query = $this->db->where('id', $usid)->update('crud', $data);
        }

        if ($sql_query) {
            $message = ($usid > 0) ? "updated" : "inserted";
            $this->session->set_flashdata('success', 'Record successful ' . $message);
            return true;
        } else {
            $this->session->set_flashdata('error', 'Somthing went worng. Error!!');
            return false;
        }
    }

    public function deleterow($uid) {
        $sql_query = $this->db->where('id', $uid)->delete('crud');
        if ($sql_query) {
            $this->session->set_flashdata('success', 'Record delete successfully');
            return true;
        } else {
            $this->session->set_flashdata('error', 'Somthing went worng. Error!!');
            return false;
        }
    }

}
