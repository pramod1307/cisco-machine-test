<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RouterDetails extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Router_Model');
    }

    /*
     * get all records
     */

    public function index() {
        $where = [];
        $isSearch = false;
        if ($this->input->is_ajax_request()) {
            $searchKey = $this->input->get("q", TRUE);
            if (!empty($searchKey)) {
                $where["sapid"] = $searchKey;
                $where["mac_address"] = $searchKey;
                $where["loopback"] = $searchKey;
                $where["hostname"] = $searchKey;
            }
            $isSearch = true;
        }

        $results = $this->Router_Model->getdata($where);

        if (!$isSearch) {
            $this->load->view('read', ['result' => $results]);
        } else {
            echo json_encode($results);
            exit();
        }
    }

    /*
     * get single row records
     */

    public function getdetails($uid) {
        $reslt = $this->Router_Model->getuserdetail($uid);
        $this->load->view('update', ['row' => $reslt]);
    }

    /*
     * save and update into database
     */

    public function save() {

        $this->form_validation->set_rules('sapid', 'sapid', 'required');
        $this->form_validation->set_rules('hostname', 'hostname', 'required');
        $this->form_validation->set_rules('loopback', 'loopback', 'required');
        $this->form_validation->set_rules('mac_address', 'mac_address', 'required');

        if ($this->form_validation->run()) {
            $sapid = $this->input->post('sapid');
            $hostname = $this->input->post('hostname');
            $loopback = $this->input->post('loopback');
            $mac_address = $this->input->post('mac_address');

            $usid = $this->input->post('userid');
            $usid = (!empty($usid) && is_numeric($usid)) ? $usid : 0;

            $data = array(
                'sapid' => $sapid,
                'hostname' => $hostname,
                'loopback' => $loopback,
                'mac_address' => $mac_address
            );

            $response = $this->Router_Model->insertUpdateData($data, $usid);
            if ($response) {
                redirect("/");
            }
            $this->load->view('insert');
        } else {
            $this->load->view('insert');
        }
    }

    /*
     * delete row into database
     */

    public function deleteRow($uid) {
        $response = $this->Router_Model->deleterow($uid);
        redirect("/");
    }

}
