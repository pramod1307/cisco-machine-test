<?php

require APPPATH . 'libraries/REST_Controller.php';

class Router extends REST_Controller {

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_get($sapid = 0) {
        if (!$this->_prepare_basic_auth()) {
            $this->response(["error" => "Unauthorized error."], REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        if (!empty($sapid)) {
            $data = $this->db->get_where("crud", ['sapid' => $sapid])->row_array();
        } else {
            $data = $this->db->get("crud")->result();
        }

        $this->response($data, REST_Controller::HTTP_OK);
    }

    /*
     * get all ivp4 based on ip
     */

    public function index_get_ivpDetailsIpRange($ipStart,$ipEnd) {
        if (!$this->_prepare_basic_auth()) {
            $this->response(["error" => "Unauthorized error."], REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        if (!empty($sapid)) {
            $this->db->where('(INET_ATON(mac_address) BETWEEN INET_ATON("'.$ipStart.'") AND INET_ATON("'.$ipEnd.'"))');
            $data = $this->db->get_where("crud")->row_array();
        } else {
            $data = $this->db->get("crud")->result();
        }

        $this->response($data, REST_Controller::HTTP_OK);
    }

    /**
     * post data from this method.
     *
     * @return Response
     */
    public function index_post() {
        if (!$this->_prepare_basic_auth()) {
            $this->response(["error" => "Unauthorized error."], REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $input = $this->input->post();
        if(empty($input['sapid']) || empty($input['loopback']) || empty($input['hostname']) || empty($input['mac_address'])) {
            $this->response(['Invalid request.'], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }
        
        
        if ($this->checkSapiIdHostname($input) <= 0) {
            $this->db->insert('crud', $input);
            $this->response(['Router created successfully.'], REST_Controller::HTTP_OK);
        } else {
            $this->response(['Please enter unique hostname or sapid.'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    private function checkSapiIdHostname($input) {

        $this->db->or_where(['loopback' => $input["loopback"], 'hostname' => $input["hostname"]]);
        $data = $this->db->get_where("crud")->row_array();

        return count($data);
    }

    /**
     * update from this method.
     *
     * @return Response
     */
    public function index_put($ip) {

        if (!$this->_prepare_basic_auth()) {
            $this->response(["error" => "Unauthorized error."], REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $input = $this->put();
        $response = $this->db->update('crud', $input, array('mac_address' => $ip));
        if ($response) {
            $this->response(['Router updated successfully.'], REST_Controller::HTTP_OK);
        } else {
            $this->response(['Record not saved.'], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_delete($ip) {
        if (!$this->_prepare_basic_auth()) {
            $this->response(["error" => "Unauthorized error."], REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $response = $this->db->delete('crud', array('mac_address' => $ip));
        if($response) {
            $this->response(['Router deleted successfully.'], REST_Controller::HTTP_OK);
        } else {
            $this->response(['Invalid request.'], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    protected function _prepare_basic_auth() {
        // If whitelist is enabled it has the first chance to kick them out
        if ($this->config->item('rest_ip_whitelist_enabled')) {
            $this->_check_whitelist_auth();
        }

        // Returns NULL if the SERVER variables PHP_AUTH_USER and HTTP_AUTHENTICATION don't exist
        $username = $this->input->server('PHP_AUTH_USER');
        $http_auth = $this->input->server('HTTP_AUTHENTICATION');
        $password = NULL;
        if ($username !== NULL) {
            $password = $this->input->server('PHP_AUTH_PW');
        } elseif ($http_auth !== NULL) {
            // If the authentication header is set as basic, then extract the username and password from
            // HTTP_AUTHORIZATION e.g. my_username:my_password. This is passed in the .htaccess file
            if (strpos(strtolower($http_auth), 'basic') === 0) {
                // Search online for HTTP_AUTHORIZATION workaround to explain what this is doing
                list($username, $password) = explode(':', base64_decode(substr($this->input->server('HTTP_AUTHORIZATION'), 6)));
            }
        }

        return $this->_check_login($username, $password);
    }

}
