<?php require 'layouts/header.php'; ?>

<div class="row">
    <div class="col-md-12">
        <h3>CRUD Operations using CodeIgniter</h3> <hr />
        <!--- Success Message --->
        <?php if ($this->session->flashdata('success')) { ?> 
            <p style="font-size: 20px; color:green"><?php echo $this->session->flashdata('success'); ?>
            </p>
        <?php } ?>
        <!---- Error Message ---->
        <?php if ($this->session->flashdata('error')) { ?>
            <p style="font-size: 20px; color:red"><?php echo $this->session->flashdata('error'); ?></p>
        <?php } ?> 
        <div class="col-md-6">
            <a href="<?php echo site_url('insert'); ?>" class="btn btn-primary"> Insert Record</a>
        </div>
        <div class="col-md-6">
            <input type="text" name="search" placeholder="search" id="search-router" class="form-control pull-right col-md-3" />
        </div>
        <div class="clearfix"></div>

        <div class="table-responsive">                
            <table id="mytable" class="table table-bordred table-striped">                 
                <thead>
                <th>#</th>
                <th>Sap Id</th>
                <th>Host Name</th>
                <th>LoopBack</th>
                <th>Mac Address</th>
                <th>Created Date</th>
                <th>Updated Date</th>
                <th>Edit</th>
                <th>Delete</th>
                </thead>
                <tbody id="routerHtmlBody">    
                    <?php
                    if (!empty($result)) {
                        $cnt = 1;
                        foreach ($result as $row) {
                            ?>  
                            <tr>
                                <td><?php echo htmlentities($cnt++); ?></td>
                                <td><?php echo htmlentities($row->sapid); ?></td>
                                <td><?php echo htmlentities($row->hostname); ?></td>
                                <td><?php echo htmlentities($row->loopback); ?></td>
                                <td><?php echo htmlentities($row->mac_address); ?></td>
                                <td><?php echo htmlentities($row->created_at); ?></td>
                                <td><?php echo htmlentities($row->updated_at); ?></td>
                                <td>
                                    <?php
//for passing row id to controller
                                    echo anchor("RouterDetails/getdetails/{$row->id}", ' ', 'class="btn btn-primary btn-xs glyphicon glyphicon-pencil"')
                                    ?>
                                </td>
                                <td>
                                    <?php
//for passing row id to controller
                                    echo anchor("RouterDetails/deleteRow/{$row->id}", ' ', 'class="glyphicon glyphicon-trash btn-danger btn-xs"')
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="9" style="text-align: center">No record found</td></tr>
                    <?php } ?>
                </tbody>      
            </table>
        </div>
    </div>
</div>


<script src="<?php echo base_url('/assests/js/routers.js'); ?>"></script>
<?php require 'layouts/footer.php'; ?>