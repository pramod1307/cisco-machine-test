<?php require 'layouts/header.php'; ?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Update Record |  CRUD Operations using CodeIgniter</h3>
            <hr />
        </div>
    </div>
    <!--- Success Message --->
    <?php if ($this->session->flashdata('success')) { ?> 
        <p style="font-size: 18px; color:green"><?php echo $this->session->flashdata('success'); ?></p>
    <?php } ?>
    <!---- Error Message ---->
    <?php if ($this->session->flashdata('error')) { ?>
        <p style="font-size: 18px; color:red"><?php echo $this->session->flashdata('error'); ?></p>
    <?php } ?> 	
        
    <?php echo form_open('RouterDetails/save', ['name' => 'routerForm', 'autocomplete' => 'off']); ?>
    <?php echo form_hidden('userid', $row->id); ?>
    <div class="row">
        <div class="col-md-4"><b>sapid</b>
            <?php echo form_input(['name' => 'sapid', 'class' => 'form-control', 'value' => set_value('sapid', $row->sapid)]); ?>	
            <?php echo form_error('sapid', "<div style='color:red'>", "</div>"); ?>
        </div>
        <div class="col-md-4"><b>hostname</b>
            <?php echo form_input(['name' => 'hostname', 'class' => 'form-control', 'value' => set_value('hostname', $row->hostname)]); ?>	
            <?php echo form_error('hostname', "<div style='color:red'>", "</div>"); ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-4"><b>Email id</b>
            <?php echo form_input(['name' => 'loopback', 'class' => 'form-control', 'value' => set_value('loopback', $row->loopback)]); ?>	
            <?php echo form_error('loopback', "<div style='color:red'>", "</div>"); ?>	
        </div>
        <div class="col-md-4"><b>mac_address</b>
            <?php echo form_input(['name' => 'mac_address', 'class' => 'form-control', 'value' => set_value('mac_address', $row->mac_address)]); ?>	
            <?php echo form_error('mac_address', "<div style='color:red'>", "</div>"); ?>	
        </div>
    </div>

    <div class="row" style="margin-top:1%">
        <div class="col-md-8">
            <?php echo form_submit(['name' => 'insert', 'value' => 'Update']); ?>	
        </div>
    </div>     
    <?php echo form_close(); ?>       
</div>
<?php require 'layouts/footer.php'; ?>