<?php require 'layouts/header.php'; ?>
<div class="row">
<div class="col-md-12">
<h3>Insert | CRUD Operations using CodeIgniter</h3>
<hr />
</div>
</div>

<?php echo form_open('RouterDetails/save', ['name' => 'routerForm', 'autocomplete' => 'off']); ?>
<div class="row">
    <div class="col-md-4"><b>SapiId</b>
        <?php echo form_input(['name' => 'sapid', 'class' => 'form-control', 'value' => set_value('sapid')]); ?>
        <?php echo form_error('sapid', "<div style='color:red'>", "</div>"); ?>
    </div>
    <div class="col-md-4"><b>Host Name</b>
        <?php echo form_input(['name' => 'hostname', 'class' => 'form-control', 'value' => set_value('hostname')]); ?>
        <?php echo form_error('hostname', "<div style='color:red'>", "</div>"); ?>

    </div>
</div>
<div class="row">
    <div class="col-md-4"><b>Loopback</b>
        <?php echo form_input(['name' => 'loopback', 'class' => 'form-control', 'value' => set_value('loopback')]); ?>	
        <?php echo form_error('loopback', "<div style='color:red'>", "</div>"); ?>	
    </div>
    <div class="col-md-4"><b>mac Address</b>
        <?php echo form_input(['name' => 'mac_address', 'class' => 'form-control', 'value' => set_value('mac_address')]); ?>
        <?php echo form_error('mac_address', "<div style='color:red'>", "</div>"); ?>	
    </div>
</div>

<div class="row" style="margin-top:1%">
    <div class="col-md-8">
        <?php echo form_submit(['name' => 'insert', 'value' => 'Submit']); ?>	
    </div>
</div> 

<?php echo form_close(); ?>       

<?php require 'layouts/footer.php'; ?>