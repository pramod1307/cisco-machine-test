CREATE TABLE IF NOT EXISTS `crud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sapid` varchar(18) NOT NULL,
  `hostname` varchar(14) NOT NULL,
  `loopback` varchar(4) NOT NULL,
  `mac_address` varchar(17) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

ALTER TABLE crud ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT NOW();

ALTER TABLE crud ADD COLUMN updated_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';



****BASIC AUTH****
username: admin
password: 1234


****POST REQUEST****
URL: http://localhost/csc/crud/index.php/api/Router
Request will be:

sapid:abc
hostname:abc
loopback:abc
mac_address:192.168.1.1

****PUT REQUEST****
URL: http://localhost/csc/crud/index.php/api/Router/$ip_address
Request will be:
sapid:abc


****GET REQUEST****
URL: http://localhost/csc/crud/index.php/api/Router/$ip_address

****DELETE REQUEST****
URL: http://localhost/csc/crud/index.php/api/Router/$ip_address

****GET REQUEST BASED ON IP RANGE****
URL: http://localhost/csc/crud/index.php/api/Router/ivpDetailsIpRange/$ipStart/$ipEnd