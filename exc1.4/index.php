<?php

function drawShape($shape) {
    header('Content-type: image/png');
    $png_image = imagecreate(300, 300);
    $grey = imagecolorallocate($png_image, 229, 229, 229);
    $green = imagecolorallocate($png_image, 128, 204, 204);
    imagefilltoborder($png_image, 0, 0, $grey, $grey);


    $poly_points = array(150, 200, 100, 280, 200, 280);
    imagefilledpolygon($png_image, $poly_points, 3, $green);      // POLYGON

    imagepng($png_image);
    imagedestroy($png_image);
}

function main() {
    drawShape("RECTANGLE");
}

main();
