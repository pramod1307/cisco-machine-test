<?php

ini_set("display_errors", "on");
error_reporting(E_ALL);
//create view in database

require 'database.class.php';

function createView() {
    $db = new Database;
    
    $view = getViewQuery();
    
    $db->query($view);

    return $db->execute();
}

function getViewQuery() {
    return "CREATE OR REPLACE VIEW RouterDetails AS SELECT id, sapid, hostname, loopback, mac_address, created_at, updated_at FROM crud";
}

function main() {
    if (createView())
        die("View successfully created");
    else
        die("Error getting in view ceation");
}

main();
