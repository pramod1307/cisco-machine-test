<?php
require 'server.php';

if (!empty($success)) {
    ?>
    <p class="success text-center">
        Files uploaded successfully and compressed into a zip format
    </p>
    <p class="success text-center">
        <a href="pdf_store/<?php echo $success; ?>" target="__blank">Click here to download the zip file</a>
    </p>
    <?php
}
?>
<form name="upload_files" action="" method="post" enctype="multipart/form-data">
    <input name="attechment[]" multiple="multiple" type="file" class="logininputtext1" id="ticket_file" style='width: 250px; color:#000;'  placeholder="Upload File"  autocomplete="off" />
    <input type="submit" name="Submit" value="Submit" />
</form>