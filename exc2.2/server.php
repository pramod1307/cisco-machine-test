<?php

/* * ***
  We must be need to create two folder in same directory
  1. attchments
  2. pdf_store
  attchments folder we can understand like a temporary folder for create zip folder of all files.
 * * */

error_reporting(E_ALL);
ini_set('display_errors', '1');

$files = '';
$zip_directory = '';
$success = '';

if (isset($_POST['Submit'])) {

    /* creates a compressed zip file */

    function create_zip($files = array(), $destination = '', $overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if (file_exists($destination) && !$overwrite) {
            return false;
        }
        //vars
        $valid_files = array();
        //if files were passed in...
        if (is_array($files)) {
            //cycle through each file
            foreach ($files as $file) {
                //make sure the file exists
                if (file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have valid files...
        if (count($valid_files)) {
            //create the archive
            $zip = new ZipArchive();
            if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            //add the files
            foreach ($valid_files as $file) {
                $zip->addFile($file, $file);
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);
        } else {
            return false;
        }
    }

    foreach ($_FILES["attechment"]["name"] as $key_value => $file_value) {
        $target_path = dirname(__FILE__) . '/attchments/';
        $target_path = $target_path . basename($_FILES['attechment']['name'][$key_value]);

        $files .= 'attchments/' . $_FILES['attechment']['name'][$key_value] . ',';

        if (move_uploaded_file($_FILES['attechment']['tmp_name'][$key_value], $target_path)) {

            //echo "The file ".  basename( $_FILES['attechment']['name'][$key_value]). " has been uploaded";
        } else {

            //echo "There was an error uploading the file, please try again!";
            //exit;
        }
    }

    $files = substr($files, 0, -1);
    $files_to_zip = explode(',', $files);
    $zip_directory = md5(rand() * time()) . '.zip';
    //if true, good; if false, zip creation failed
    $result = create_zip($files_to_zip, 'pdf_store/' . $zip_directory);
    if ($result) {
        $success = $zip_directory;
    }

    /* if(copy($zip_directory, 'pdf_store/'.$zip_directory)) { 
      echo "success";
      } else {
      echo "Failed";
      } */
    //unlink('myarchive.zip');
}
?>