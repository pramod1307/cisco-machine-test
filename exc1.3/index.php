<?php

if (PHP_SAPI != "cli") {
    exit;
}

require 'database.class.php';

class uniqueEntries {
    private $db;

    function saveUniqueData() {
        $this->db = new Database;
        
        $this->db->query('INSERT INTO crud(sapid, hostname, loopback, mac_address) VALUES ("'. uniqid().'", "'. uniqid().'", "'. uniqid().'", "'. uniqid().'")'); 
        
        return $this->db->execute();
    }

    function process($n) {
        $insertData = [];
        for ($i = 1; $i <= $n; $i++) {
            $this->saveUniqueData();
        }
    }

}

function main() {

    if (!empty($_SERVER['argv'])) {
        /* for ($i = 0; $i < $argc; $i++) {
          echo "Argument #" . $i . " - " . $argv[$i] . "\n";
          } */
        $obj = new uniqueEntries();
        
        $number = $_SERVER['argv'][1];
        $obj->process($number);
        
    } else {
        echo "argc and argv disabled\n";
    }
}

main();
